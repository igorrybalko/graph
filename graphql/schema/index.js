const { buildSchema } = require('graphql')

module.exports = buildSchema(`
     type Book {
    id: Int!
    title: String!
    description: String!
    author: Int!
    year: Int!
    categories: String!
   }
   
   input BookInput {
        title: String!
        description: String!
        author: Int!
        year: Int!
        categories: String!
   }

  type RootQuery {
    hello: String
    books: [Book]
  }
  
   type RootMutation {
    createBook(bookInput: BookInput): String
  }
  
  schema {
    query: RootQuery
    mutation: RootMutation
  }
`)