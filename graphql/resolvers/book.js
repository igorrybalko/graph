const db = require('./models')

module.exports = {
    books: () => {
        return db.Book.findAll({
            attributes: ['id', 'title', 'description', 'author', 'year', 'categories']
        })
    },
    createBook: args => {

        return new Promise(resolve => {
            db.Book.create({
                title: args.bookInput.title,
                description: args.bookInput.description,
                author: +args.bookInput.author,
                year: +args.bookInput.year,
                categories: args.bookInput.categories
            }).then(()=>{
                resolve('Success')
            })
        })

    }

}