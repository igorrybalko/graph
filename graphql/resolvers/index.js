const bookResolver = require('./book')

module.exports = {
    hello: () => {
        return 'Hello world!'
    },
    ...bookResolver
}