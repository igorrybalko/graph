require('dotenv').config()

const express = require('express'),
    bodyParser = require('body-parser'),
    helmet = require('helmet'),
    conf = require('./config'),
    logger = require('morgan'),
    graphqlHTTP = require('express-graphql'),

    grahpqlSchema = require('./graphql/schema'),
    grahpqlResolvers = require('./graphql/resolvers')

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(helmet())
app.use(logger('dev'))

app.use('/graphql', graphqlHTTP({
    schema: grahpqlSchema,
    rootValue: grahpqlResolvers,
    graphiql: true,
}))

app.listen(process.env.PORT || '3001')
