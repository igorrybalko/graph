const Sequelize = require('sequelize'),
    conf = require('../config')

module.exports = new Sequelize(conf.get('db-name'), conf.get('db-user'), conf.get('db-password'), {
    host: 'localhost',
    dialect: 'mysql'
})
