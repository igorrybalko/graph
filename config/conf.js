module.exports = {
  "domain": "localhost",
  "db-name": process.env.DB_NAME,
  "db-user": process.env.DB_USER,
  "db-password": process.env.DB_PASSWORD,
  "secretJwt": process.env.SECRET_JWT,
  "session": {
    "secret": "secretWorld",
    "key": "NODESESSID",
    "cookie": {
      "path": "/",
      "maxAge": null,
      "httpOnly": true
    }
  },
  "cors": ["http://localhost:3002"]
};
