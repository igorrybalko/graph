const nconf = require('nconf');
nconf.argv()
    .env()
    .defaults(require('./conf'));

module.exports = nconf;
